# Shards

Observer 

```
13:06:31|~/demo/customer-demo> oc get IngressController -n openshift-ingress-operator
NAME      AGE
default   26d
```

Appliquer les label selon la configuration voulue

Appliquer le label dev-ingress sur infra3 et prod-ingress sur infra1 et infra2
```
oc label node infra3.openshift.rhcasalab.com dev-ingress=

oc label node infra1.openshift.rhcasalab.com prod-ingress=
oc label node infra2.openshift.rhcasalab.com prod-ingress=
```

Modifier le ingress controller actuel pour utiliser les prod-ingress

```
oc edit IngressController default -n openshift-ingress-operator
```

Appliquer le nouveau ingress controller

```
oc apply -f demo10/manifests/dev-ingress.yaml
```

Observer les ingress-controller

```
13:20:15|~/demo/customer-demo> oc get IngressController -n openshift-ingress-operator
NAME          AGE
default       26d
dev-ingress   33s
```

Observer les pods ingress

```

```

Créer un nouveau projet qui sera associé à l'ingress dev

```
oc label namespace demo10 dev-namespace=

```

Appliquer les fichiers deployment, service et routes sur le nouveau namespace

```
oc apply -f demo10/manifests/deployment.yaml
oc apply -f demo10/manifests/service.yaml
oc apply -f demo10/manifests/route.yaml
```

Observer dans la console UI la route demo10 qui est admin sur le router dev-ingress

```
Ouvrir le lien, fonctionnel mais mauvais certificat. 
```

Installer le certificat pour le ingress

```
oc patch ingresscontroller.operator dev-ingress \
     --type=merge -p \
     '{"spec":{"defaultCertificate": {"name": "devcert"}}}' \
     -n openshift-ingress-operator
```
