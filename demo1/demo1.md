# Demo 1 - Installation IPI et mise à niveau

## Préparation 

Pré-créer 1 clusters dans ACM:
  AWS
  region: east-1
	release: quay.io/openshift-release-dev/ocp-release:4.5.15-x86_64

Pré-créer 1 clusters dans ACM: (pour demo OCS)
  AWS
  region: ca-central-1
	release: quay.io/openshift-release-dev/ocp-release:4.5.15-x86_64

## Demo

- Créer un cluster à partir de ACM dans Azure 
  region: canadacentral
  release: quay.io/openshift-release-dev/ocp-release:4.6.7-x86_64

- Le cluster aws aws-ca affichera update disponible, cliquer sur Update

- Observer le controlleur Ingress aws-east

```
oc get IngressController -n openshift-ingress-operator
```

- Modifier le controlleur Ingress replica a 3 

```
oc edit IngressController -n openshift-ingress-operator
```
