# Demo 4 - Gestion des accès - Role Based Access Control (RBAC)

Ref: 
https://docs.openshift.com/container-platform/4.6/authentication/using-rbac.html

### Pre-requis

Dans SSO, usagers suivants
- demo-ops
- demo-dev

Appliquer les policies ACM pour créer les namespace et les Roles

```
oc apply -f demo4/policies
```

Créer un service account dans chaque projets

- demo-robot

## Demo

appliquer le label demo4="" sur le cluster

Connecter avec usager demo-dev

Explorer la liste des projets
- demo4-rbac-dev
- demo4-rbac-prod

Démontrer que l'usager dev peut modifier le projet demo4-rbac-dev mais pas le demo4-rbac-prod

Démontrer que l'usager dev peut afficher la liste des secrets en production mais pas afficher son contenu

===

Connecter avec l'usager demo-ops

Démontrer que l'usager ops ne peut voir aucun secret ni en prod, ni en dev. 

Démontrer que l'usager ops peut redémarrer un POD, mais ne peut modifier le contenu. 

===

Tenter de modifier le clusterrole ops-prod pour donner les droits de consulter les secrets

Observer ACM qui remmet en place le Role automatiquement

===

Automatisation avec un CICD pipeline

Récupérer le secret robot-prod-token-

```
oc login --token=
```

Tenter de 
- list POD
- détruire POD
- list deploy
- creer deploy

```
oc apply -f demo4/manifests/robot/deployment-app3-from-robot.yaml
```

Tenter de lister secret
Tenter de creer secret

```
oc create -f demo4/manifests/robot/app3-secret.yaml
```

