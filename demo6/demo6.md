# Stockage intégré - Openshift Container Storage (OCS)

## Pre-requis

1 Cluster Openshift avec OCS sur AWS

Assigner le label demo6= sur le cluster pour déployer l'application ubi

## Demo

### Tour de roue de l'interface graphique

### Tour de roue du concept OCS, Rook.io, Noobaa 

### Importance de ReadWriteMany

- Observer le comportement d'un déploiement d'application 3 réplicas sur cloud publique ubi2 vs ubi3

```
oc get pods -o wide
```

Connecter dans les pods ubi3, écrire un fichier dans /volume et démontrer
le fichier dans les autres pods. 

### Démontrer ajout de stockage sur deployment via la console web (ubi1)

```
oc apply -f demo6/manuel/deployment-ubi1.yaml
```

Aller à la console Developpeur et ajouter un nouveau volume au deployment ubi1

Se connecter au POD et montrer le volume df -ha

Créer un fichier de grande taille pour montrer alertage et expansion'

```
dd if=/dev/zero of=1g.img bs=1 count=0 seek=4G
```

Montrer l'utilisation dans le graphique Storage. Aller dans Prometheus voir plus de détails 

Observer l'alerte
PVC demo6-ubi1 utilization has crossed 75%. Free up some space or expand the PVC.

### Démontrer l'expension dynamique des volumes 

Afficher l'état du volume

Expand et démontrer le changement live

Démontrer que le POD ne redémarre pas. Le changement est live.


### Object Bucket

Créer un object bucket claim a partir de la console

Montrer les informations S3 

Depuis le pod awscli

```
aws configure
```

Afficher la liste des buckets pour la clé courante
```
aws s3 --endpoint-url=https://s3-openshift-storage.apps.openshift.rhcasalab.com ls
```

### Mount informations S3 sur deployment

Montrer YAML du deployment et du obc et la relation entre les deux

Terminal sur pod uploader

Montrer le bucket vide
```
aws s3 --endpoint-url $BUCKET_HOST ls $BUCKET_NAME
```

Copier un fichier sur le bucket
```
aws s3 --endpoint-url $BUCKET_HOST cp /content/logo1.png s3://$BUCKET_NAME
```

Aller dans Noobaa, configurer le bucket uploader pour répliquer sur AWS, Azure et Google. 

Retourner au POD et copier un autre fichier pour montrer la réplication active. 

```
aws s3 --endpoint-url $BUCKET_HOST cp /content/logo2.png s3://$BUCKET_NAME
```


### CodeReady Python
