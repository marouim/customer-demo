from s3_client import s3
import sys
import json

bucket = "test123-1e236140-88a5-42cf-b5e2-f52f86c6c175"
object_name = "test156"

object_to_write = {
  "name": "name of this object",
  "value": "value123",
  "meta": "meta123"
}

s3.put_object(Bucket=bucket, Key=object_name, Body=json.dumps(object_to_write))
