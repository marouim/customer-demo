# Demo3 - Securité

## Préparation

- Login avec user mouimet-dev

- Créer projet webroot

```
oc new-project demo3-webroot
```

- Créer les manifests

```
oc apply -f demo3/manifests/webroot
```

- Lancer build le répertoire container-root

```
oc start-build webroot
```

Appliquer les policies ACM pour surveiller le SCC restricted

```
oc apply -f demo3/policies
```

## Démo

appliquer le label demo3="" sur le cluster

- Creer le deployment de webroot

```
oc apply -f demo3/manifests/webroot/deployment.yaml
```

- Observer le comportement, crashloopback, logs

- Lancer le même conteneur avec un terminal

```
oc apply -f demo3/manifests/webroot/deployment-terminal.yaml
```

- Tenter de démarrer le serveur web manuellement 

```
apachectl -D FOREGROUND
```

- Observer l'erreur de permissions sur /run/httpd

- Observer le UID dynamique

```
sh-4.2$ whoami
1001030000
```

- Observer la section openshift.io/sa.scc.uid-range: du projet demo3-webroot


## Tenter de contourner

Demander a un admin de modifier la scc restricted pour permettre anyuid 

```
oc login (user admin)

oc edit scc restricted 
```

Remplacer
```
runAsUser:
  type: MustRunAsRange
```
Par
```
runAsUser:
  type: RunAsAny
```

- Login en tant que utilisateurs

- Tenter de démarrer le Pod 

- Aller dans ACM, menu Gouvernance and Risk

- Observer la policy restricted-scc , elle sera en violation apres 30 sec. 

- Changer la policy a enforce 

- Observer le SCC lorsque ACM a mitige le probleme. 

- Tenter de redémarrer le Pod webroot, il retombe en crash loopback. 


### Démarrer une version sécuritaire

- Démarrer une version sécuritaire de httpd en utilisant l'image de redhat

```
oc apply -f demo3/manifests/webclean
```