## Pre-requis

Creer namespace demo11

```
oc new-project demo11
```

Détruire ressources existantes si nécessaire:
- BC, IS, Pipeline, Secret Git


Developer Console

Add From Git
URL: https://github.com/marouim/gestiontransaction.git

Le builder trouve automatiquement le bon code source .NET Core dans ce cas

Application name: gestiontransaction

Choisir Knative Service

Cocher Add pipeline

Attendre le build et deployment.

Tester la route. 

Aller dans Pipelines, cliquer Add Trigger
github-push
Minor: 1
PVC: build-workspace

Configurer le Webhook dans Github

=

Dans CodeReady, faire une modification au code et commmit+push

Observer le pipeline triggé par Github

Une fois build complété, observer la nouvelle révision

Démontrer le split de traffic 50/50 entre révisions

=

Ajouter un cluster Kafka via console développeur

Créer un Topic Kafka nommé transactions via console

Créer un EventSource  Kafka vers l'app serverless

=

Ouvrir Logging console Kibana

= 

Ouvrir un terminal sur un pod Kafka

Démarrer un producer 

bin/kafka-console-producer.sh --topic transactions --bootstrap-server localhost:9092


= 
Configurer KNative 

```
spec:
  template:
    metadata:
      annotations:
        autoscaling.knative.dev/target: "2"
        autoscaling.knative.dev/metric: "concurrency"
        autoscaling.knative.dev/window: 10s
        autoscaling.knative.dev/minScale: "1"
        autoscaling.knative.dev/maxScale: "10"
      name: gestiontransaction-ttmmp-X
```
* Le name doit être différent (incrément numéro)












