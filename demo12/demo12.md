# Data integration 

## Pre-requis

Base de données mariadb

table transaction (transaction.sql)

Integration FuseOnline avec le swagger (api-swagger.yaml) pour getList et Post (new)

## Demo

#### Observations

Observer l'intégration actuelle

Observer les mapping 

Tester l'URL et voir la BD

#### Ajouter une étape dans l'intégration

Creer une nouvelle connexion Kafka vers le nouveau cluster Kafka créé au demo serverless

my-cluster-kafka-bootstrap.demo11:9092

Ajouter une étape d'intégration Kafka

JSON instance
```
{
  "transaction_id": "R",
  "transaction_date": 54344,
  "transaction_type": "achat",
  "transaction_amount": 455.55
}
```

Observer le flag datamapping et ajouter le mapping

Une fois deployé, faire des tests de POST et observer le logger et serverless Pods.




